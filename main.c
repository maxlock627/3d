#include <stdio.h>
#include <raylib.h>
#include <raymath.h>

#define SCREEN_WIDTH 1280
#define SCREEN_HEIGHT 720

int main() {
	InitWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "3D");
	SetTargetFPS(60);

	const int mode = CAMERA_FIRST_PERSON;
	const Vector3 origin = {0.0f, 0.0f, 0.0f};

	Vector3 player_pos = {-1.0f, 2.0f, -1.0f};
	Vector3 player_size = {1.0f, 2.0f, 1.0f};

	Camera camera;
	camera.position   = player_pos;
	camera.target     = (Vector3){5.0f, 2.0f, 5.0f};
	camera.up         = (Vector3){0.0f, 2.0f, 0.0f};
	camera.fovy       = 60.0f;
	camera.projection = CAMERA_PERSPECTIVE;

	Vector3 cube_pos  = {5.0f, 1.0f, 5.0f};
	Vector3 cube_size = {5.0f, 2.0f, 5.0f};

	Color cube_color = GREEN;
	
	DisableCursor();
	while(!WindowShouldClose()) {
		if (IsKeyDown(KEY_W)) {
			// move in the target direction
			player_pos.z += 0.2f;
		}
		if (IsKeyDown(KEY_S)) {
			// move backwards int the target direction
			player_pos.z -= 0.2f;
		}

		Vector3 old = camera.position;

		// UpdateCamera(&camera, mode);
		UpdateCameraPro(&camera,
						(Vector3){0},
						(Vector3){
							GetMouseDelta().x*0.05f,
							GetMouseDelta().y*0.05f,
							0.0f},
						0.0f);
		camera.position = player_pos;

		// check collision player vs cube
		if (CheckCollisionBoxes(
				(BoundingBox){
					(Vector3){
						player_pos.x - player_size.x/2,
						player_pos.y - player_size.y/2,
						player_pos.z - player_size.z/2,},
					(Vector3){
						player_pos.x + player_size.x/2,
						player_pos.y + player_size.y/2,
						player_pos.z + player_size.z/2,}},
				(BoundingBox){
					(Vector3){
						cube_pos.x - cube_size.x/2,
						cube_pos.y - cube_size.y/2,
						cube_pos.z - cube_size.z/2,},
					(Vector3){
						cube_pos.x + cube_size.x/2,
						cube_pos.y + cube_size.y/2,
						cube_pos.z + cube_size.z/2}})){
			cube_color = RED;
			camera.position = old;
		} else {
			cube_color = GREEN;
		}
		
		BeginDrawing();
		{
			ClearBackground(BLACK);
			BeginMode3D(camera);
			{
				DrawLine3D(origin, (Vector3){0.0f, 20.0f, 0.0f}, WHITE);
				DrawCubeV(cube_pos, cube_size, cube_color);
				DrawGrid(50, 1);
			}
			EndMode3D();
		}
		EndDrawing();
	}
	CloseWindow();
	return 0;
}
